package cz.fi.muni.pa165.tasks;

import cz.fi.muni.pa165.PersistenceSampleApplicationContext;
import cz.fi.muni.pa165.entity.Category;
import cz.fi.muni.pa165.entity.Product;
import cz.fi.muni.pa165.enums.Color;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.Set;


@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
public class Task02 extends AbstractTestNGSpringContextTests {

	private static Category electro = null;
	private static Category kitchen = null;
	private static Product flashlight = null;
	private static Product kitchenRobot = null;
	private static Product plate = null;

	@PersistenceUnit
	private EntityManagerFactory emf;

	@BeforeClass
	public void setup() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();

			electro = new Category();
			electro.setName("Electro");

			kitchen = new Category();
			kitchen.setName("Kitchen");

			flashlight = new Product();
			flashlight.setName("Flashlight");
			flashlight.setColor(Color.BLACK);
			flashlight.addCategory(electro);

			kitchenRobot = new Product();
			kitchenRobot.setName("Kitchen robot");
			kitchenRobot.setColor(Color.RED);
			kitchenRobot.addCategory(kitchen);

			plate = new Product();
			plate.setName("Plate");
			plate.setColor(Color.WHITE);
			plate.addCategory(kitchen);

			electro.addProduct(flashlight);
			kitchen.addProduct(kitchenRobot);
			kitchen.addProduct(plate);

			em.persist(electro);
			em.persist(kitchen);
			em.persist(flashlight);
			em.persist(kitchenRobot);
			em.persist(plate);

			em.getTransaction().commit();

		} finally {
			if (em != null) em.close();
		}
	}

	@Test
	public void testCategoryKitchen() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Category category = em.find(Category.class, kitchen.getId());
			assertContainsProductWithName(category.getProducts(), "Kitchen robot");
			assertContainsProductWithName(category.getProducts(), "Plate");
			em.getTransaction().commit();

		} finally {
			if (em != null)	em.close();
		}
	}

	@Test
	public void testCategoryElectro() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Category category = em.find(Category.class, electro.getId());
			assertContainsProductWithName(category.getProducts(), "Flashlight");
			em.getTransaction().commit();

		} finally {
			if (em != null)	em.close();
		}
	}

	@Test
	public void testProductFlashlight() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Product product = em.find(Product.class, flashlight.getId());
			assertContainsCategoryWithName(product.getCategories(), "Electro");
			em.getTransaction().commit();

		} finally {
			if (em != null)	em.close();
		}
	}

	@Test
	public void testProductKitchenRobot() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Product product = em.find(Product.class, kitchenRobot.getId());
			assertContainsCategoryWithName(product.getCategories(), "Kitchen");
			em.getTransaction().commit();

		} finally {
			if (em != null)	em.close();
		}
	}

	@Test
	public void testProductPlate() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Product product = em.find(Product.class, plate.getId());
			assertContainsCategoryWithName(product.getCategories(), "Kitchen");
			em.getTransaction().commit();

		} finally {
			if (em != null)	em.close();
		}
	}

	@Test(expectedExceptions= ConstraintViolationException.class)
	public void testDoesntSaveNullName() {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Product product = new Product();
			product.setName(null);
			em.persist(product);
			em.getTransaction().commit();
		} finally {
			if (em != null) em.close();
		}
	}


		private void assertContainsCategoryWithName(Set<Category> categories, String expectedCategoryName) {
		for(Category cat: categories){
			if (cat.getName().equals(expectedCategoryName))
				return;
		}

		Assert.fail("Couldn't find category " + expectedCategoryName + " in collection " + categories);
	}
	private void assertContainsProductWithName(Set<Product> products, String expectedProductName) {
		for(Product prod: products){
			if (prod.getName().equals(expectedProductName))
				return;
		}

		Assert.fail("Couldn't find product " + expectedProductName + " in collection " + products);
	}
}
